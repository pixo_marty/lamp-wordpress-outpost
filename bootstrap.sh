#!/usr/bin/env bash

if [ ! -e ~/.provisioned ]
then
	mkdir ~/.provisioned
fi

# update Aptitude package list and install required packages
apt-get update
	
# run provisioning scripts
for script in /vagrant/provision/*.sh
do
	if [ -f $script -a -x $script ]
	then
		$script
	fi
done
