echo "Configuring WordPress database"
wp_db_name=wordpress
wp_db_user=wp_user
mysql -uroot -pvagrant <<< "CREATE DATABASE IF NOT EXISTS $wp_db_name;"
mysql -uroot -pvagrant <<< "CREATE USER '$wp_db_user'@'localhost' IDENTIFIED BY '$wp_db_user';"
mysql -uroot -pvagrant <<< "GRANT CREATE, DELETE, SELECT, UPDATE, INSERT, ALTER, LOCK TABLES, DROP, INDEX ON $wp_db_name.* TO '$wp_db_user'@'localhost';"

