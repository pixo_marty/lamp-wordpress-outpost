wp_config_file="/var/www/html/wordpress/wp-config.php"
wp_db_name=wordpress
wp_db_user=wp_user

rm -f $wp_config_file

echo "Writing WordPress config file to $wp_config_file"

echo "<?php" >> "$wp_config_file"
echo "define('DB_NAME', '$wp_db_name');" >> "$wp_config_file"
echo "define('DB_USER', '$wp_db_user');" >> "$wp_config_file"
echo "define('DB_PASSWORD', '$wp_db_user');" >> "$wp_config_file"
echo "define('DB_HOST', 'localhost');" >> "$wp_config_file"
echo "define('DB_CHARSET', 'utf8');" >> "$wp_config_file"
echo "\$table_prefix = 'wp_';" >> "$wp_config_file"
#echo "define('WP_DEBUG', true);" >> "$wp_config_file"

echo "define('AUTH_KEY',         'put your unique phrase here');" >> "$wp_config_file"
echo "define('SECURE_AUTH_KEY',  'put your unique phrase here');" >> "$wp_config_file"
echo "define('LOGGED_IN_KEY',    'put your unique phrase here');" >> "$wp_config_file"
echo "define('NONCE_KEY',        'put your unique phrase here');" >> "$wp_config_file"
echo "define('AUTH_SALT',        'put your unique phrase here');" >> "$wp_config_file"
echo "define('SECURE_AUTH_SALT', 'put your unique phrase here');" >> "$wp_config_file"
echo "define('LOGGED_IN_SALT',   'put your unique phrase here');" >> "$wp_config_file"
echo "define('NONCE_SALT',       'put your unique phrase here');" >> "$wp_config_file"

echo "require_once(ABSPATH . 'wp-settings.php');" >> "$wp_config_file"

