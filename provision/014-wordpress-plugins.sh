wordpress_directory='/var/www/html/wordpress/wp-content/plugins'

echo "Installing plugins to $wordpress_directory"
cd "$wordpress_directory"
cp -r /vagrant/provision/wp-plugins/* "$wordpress_directory"

#activate with wp-cli
echo "Activating WP plugins"
wp plugin activate json-rest-api --allow-root

