#!/usr/bin/env bash
# Installs Composer to /usr/local/bin

if [ ! -f /usr/local/bin/composer ]
then
	curl -sS https://getcomposer.org/installer | php
	mv composer.phar /usr/local/bin/composer
fi