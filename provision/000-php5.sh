#!/usr/bin/env bash
# Installs PHP 5.x

apt-get install -y php5-cli
apt-get install -y php5-intl
apt-get install -y php5-mysql
