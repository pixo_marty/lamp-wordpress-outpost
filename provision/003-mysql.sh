#!/usr/bin/env bash
# Installs mysql-server-5.5 unattended

debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password password vagrant'
debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again password vagrant'
apt-get install -y mysql-server-5.5

service mysql start

# Create the vagrant account
mysql -uroot -pvagrant <<< "CREATE USER 'vagrant'@'localhost' IDENTIFIED BY 'vagrant';"
mysql -uroot -pvagrant <<< "GRANT CREATE ON *.* TO 'vagrant'@'localhost';"
