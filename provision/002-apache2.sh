#!/usr/bin/env bash
# Installs apache2 and mod-php

apt-get install -y apache2
apt-get install -y libapache2-mod-php5

# Enable mod_rewrite
a2enmod rewrite

# set Apache PHP timezone
sed -i "s/^;date.timezone =/date.timezone = America\/Chicago/g" /etc/php5/apache2/php.ini

# set PHP memory limit
sed -i "s/memory_limit = [0-9]*./memory_limit = 256M/" /etc/php5/apache2/php.ini

# set PHP script timeout
sed -i "s/max_execution_time = [0-9]*/max_execution_time = 60/" /etc/php5/apache2/php.ini