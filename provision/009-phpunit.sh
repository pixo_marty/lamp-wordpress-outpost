#!/usr/bin/env bash
# Install PHPUnit

#installation via pear is no longer supported as of 12/31/2014.
#pear config-set auto_discover 1
#pear install pear.phpunit.de/PHPUnit

composer global require "phpunit/phpunit=4.*"
