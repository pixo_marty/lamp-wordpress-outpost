# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = "ubuntu/trusty64"
  
  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8082" will access port 80 on the guest machine.
  config.vm.network :forwarded_port, guest: 80,   host: 8082
  config.vm.network :forwarded_port, guest: 8000, host: 8000
  config.vm.network :forwarded_port, guest: 8081, host: 8081
  config.vm.network :forwarded_port, guest: 3306, host: 3307

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network :private_network, ip: "192.168.33.10"

  # note: this should allow connecting from vagrant to the host OS
  # via 33.33.33.1. if you happen to find out why/how, let us know.
  config.vm.network :private_network, ip: "33.33.33.33"

  if !ENV["VAGRANT_DISABLE_PUBLIC_NETWORK"]
    # give a unique name & add to pixospace (10.2.2.*)
    # to simplify adding it to *.i.pixotech.com
    config.vm.hostname = "outpost-" + ENV["USER"]
    config.vm.network :public_network, :adaptor => 2
  end

  # enable provisioning with bootstrap.sh
  config.vm.provision :shell, :path => "bootstrap.sh"

  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--memory", "1536"]
  end

  # smb is slow as can be!
  if RUBY_PLATFORM =~ /darwin/ or Vagrant.has_plugin?('vagrant-winnfsd')
    config.vm.synced_folder ".", "/vagrant", type: "nfs"
  end
end
