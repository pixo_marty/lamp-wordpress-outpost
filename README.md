# README #

This project provides a basic Vagrantfile and provisioning scripts to make it convenient to set up a Wordpress install on a LAMP stack with Outpost.

### Getting Started ###

* git clone the repo
* `vagrant up`
* Navigate to http://localhost:8082/wordpress/ to set up the site
* Log in a go to Plugins in the admin panel. Activate the "WP REST API" plugin.

### Working with the WP API and Outpost

Make a HEAD request to http://localhost:8082/wordpress/ to get a link to the json api.

Use localhost:8000 to talk to outpost on the host computer

#### Ports:
```
          Vagrant            Host Machine
 +-----------------------+
 |                       |
 |  wordpress   :80   <--+--> 8082
 |    outpost   :8000 <--+--> 8000
 |      mysql   :3306 <--+--> 3307
 |                       |
 +-----------------------+
```