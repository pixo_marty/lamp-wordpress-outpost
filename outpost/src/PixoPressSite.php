<?php
namespace PixoPress;

use Outpost;
use Twig_Loader_Filesystem;
use Twig_Environment;

require_once __DIR__ . '/Responders/WordPressPostResponder.php';

class PixoPressSite extends Outpost\Site {
    private $twigLoader;
    private $twig;

    public function __construct(){
        $this->twigLoader = new Twig_Loader_Filesystem($this->templateDir());
        $this->twig = new Twig_Environment($this->twigLoader, array(
            //'cache' => $this->cacheDir(),
        ));
    }

    public function getRouter() {
        $router = parent::getRouter();
        $router->get('/', function () { print "Hello"; });
        $router->get('/article/{id:i}', new WordPressPostResponder());
        return $router;
    }

    public function renderTemplate($templateName, $data){
        echo $this->twig->render($templateName . '.html.twig', $data);
    }

    private function templateDir(){
        return __DIR__ . '/../templates';
    }

    private function cacheDir(){
        return __DIR__ . '/../cache';
    }
}