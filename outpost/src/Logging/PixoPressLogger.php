<?php
namespace PixoPress;

use Monolog;
use Monolog\Handler\StreamHandler;

class Logger
{
    private $log;

    public function __construct(){
        $this->log = new Monolog\Logger('pixo');
        $this->log->pushHandler(new StreamHandler( $this->logDir(), Monolog\Logger::DEBUG));
    }

    public function log($message){
        $this->log->addDebug($message);
    }

    private function logDir(){
        return '/vagrant/outpost/log/pixopress.log';
    }
}