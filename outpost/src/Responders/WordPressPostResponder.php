<?php
namespace PixoPress;

use Outpost;

require_once __DIR__ . '/../Resources/WordPressPostResource.php';

/*
{
  "ID": 1,
  "title": "Hello world!",
  "status": "publish",
  "type": "post",
  "author": {
        "ID": 1,
    "username": "marty@pixotech.com",
    "name": "marty@pixotech.com",
    "first_name": "",
    "last_name": "",
    "nickname": "marty@pixotech.com",
    "slug": "martypixotech-com",
    "URL": "",
    "avatar": "http://1.gravatar.com/avatar/7c21b32788f29bea47be11a3ca0dc567?s=96",
    "description": "",
    "registered": "2015-11-20T21:34:46+00:00",
    "meta": {
            "links": {
                "self": "http://localhost:8082/wordpress/?json_route=/users/1",
        "archives": "http://localhost:8082/wordpress/?json_route=/users/1/posts"
      }
    }
  },
  "content": "<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n",
  "parent": null,
  "link": "http://localhost:8082/wordpress/?p=1",
  "date": "2015-11-20T21:34:46",
  "modified": "2015-11-20T21:34:46",
  "format": "standard",
  "slug": "hello-world",
  "guid": "http://localhost:8082/wordpress/?p=1",
  "excerpt": "<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n",
  "menu_order": 0,
  "comment_status": "open",
  "ping_status": "open",
  "sticky": false,
  "date_tz": "UTC",
  "date_gmt": "2015-11-20T21:34:46",
  "modified_tz": "UTC",
  "modified_gmt": "2015-11-20T21:34:46",
  "meta": {
        "links": {
            "self": "http://localhost:8082/wordpress/?json_route=/posts/1",
      "author": "http://localhost:8082/wordpress/?json_route=/users/1",
      "collection": "http://localhost:8082/wordpress/?json_route=/posts",
      "replies": "http://localhost:8082/wordpress/?json_route=/posts/1/comments",
      "version-history": "http://localhost:8082/wordpress/?json_route=/posts/1/revisions"
    }
  },
  "featured_image": null,
  "terms": {
        "category": [
      {
          "ID": 1,
        "name": "Uncategorized",
        "slug": "uncategorized",
        "description": "",
        "taxonomy": "category",
        "parent": null,
        "count": 1,
        "link": "http://localhost:8082/wordpress/?cat=1",
        "meta": {
          "links": {
              "collection": "http://localhost:8082/wordpress/?json_route=/taxonomies/category/terms",
            "self": "http://localhost:8082/wordpress/?json_route=/taxonomies/category/terms/1"
          }
        }
      }
    ]
  }
}
*/

class WordPressPostResponder extends Outpost\Routing\Responder
{
    public function __invoke($postId){
        $resource = new WordPressPostResource($postId, []);
        $json = $this->get($resource);

        $html = $this->getSite()->renderTemplate('post', $json);

        $this->respond( $html );
    }

    /**
     * @return PixoPressSite
     */
    public function getSite(){
        return parent::getSite();
    }
}
