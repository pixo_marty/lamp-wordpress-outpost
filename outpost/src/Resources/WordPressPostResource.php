<?php
namespace PixoPress;

use Outpost;

class WordPressPostResource extends Outpost\Resources\RemoteJsonResource
{
    protected $id;

    public function __construct($id, $config) {
        $this->id = $id;
        parent::__construct($this->getRequestUrl(), $config);
    }

    /**
     * @return string
     */
    protected function getRequestUrl() {
        return "http://localhost/wordpress/?json_route=/posts/{$this->id}";
    }
}