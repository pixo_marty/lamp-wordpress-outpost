<?php

# Get the Composer autoloader
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../src/PixoPressSite.php';
require_once __DIR__ . '/../src/Logging/PixoPressLogger.php';

# Get the incoming request
$request = Symfony\Component\HttpFoundation\Request::createFromGlobals();

$logger = new PixoPress\Logger();

# Create a Site object
$site = new \PixoPress\PixoPressSite();

# Send a response
$site->respond($request);

